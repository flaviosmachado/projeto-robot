***Settings***
Documentation   Camada de Serviço do projeto de automação

Library     RequestsLibrary
Library     Collections

Resource    helpers.robot

***Variables***
${base_api_url}     http://zepalheta-api:3333 


***Keywords***
## PEGA O TOKEN QUANDO LOGA
Get Session Token
    ${resp}=     Post Session  admin@zepalheta.com.br  pwd123

    ${token}        Convert To String   Bearer ${resp.json()['token']}

    [Return]    ${token}

##LOGA /POST SESSIONS
Post Session
    [Arguments]     ${email}    ${password}

    Create Session    zp-api      ${base_api_url} 

    &{headers}=     Create Dictionary       Content-Type=application/json
    &{payload}=     Create Dictionary       email=${email}    password=${password}

    ${resp}=        POST On Session  zp-api      /sessions   json=${payload}     headers=${headers}   expected_status=anything

    [Return]        ${resp}

## CRIA UM USUARIO /POST CUSTOMER
Post Customer
    [Arguments]     ${payload}

    Create Session      zp-api      ${base_api_url}

    ${token}=       Get Session Token
    
    &{headers}=     Create Dictionary    content-type=application/json      authorization=${token}

    ${resp}=        Post On Session     zp-api      /customers      json=${payload}     headers=${headers}   expected_status=anything

    [Return]        ${resp}

## PUT CUSTOMERS
Put Customer
    [Arguments]     ${payload}  ${user_id}

    Create Session      zp-api      ${base_api_url}

    ${token}=       Get Session Token
    
    &{headers}=     Create Dictionary    content-type=application/json      authorization=${token}

    ${resp}=        Put On Session     zp-api      /customers/${user_id}     json=${payload}     headers=${headers}   expected_status=anything

    [Return]        ${resp}

## GET CUSTOMERS
Get Customers
    
    Create Session      zp-api      ${base_api_url}

    ${token}=       Get Session Token
    &{headers}=     Create Dictionary    content-type=application/json      authorization=${token}

    ${resp}=        GET On Session  zp-api      /customers   headers=${headers}   expected_status=anything

    [Return]    ${resp}

Get Unique Customer
    [Arguments]     ${user_id}

    Create Session      zp-api      ${base_api_url}

    ${token}=       Get Session Token
    &{headers}=     Create Dictionary    content-type=application/json      authorization=${token}

    ${resp}=        GET On Session  zp-api      /customers/${user_id}   headers=${headers}   expected_status=anything

    [Return]    ${resp}

## DELETA UM USUARIO
Delete Customer
    [Arguments]     ${cpf}

    ${token}=       Get Session Token
    &{headers}=     Create Dictionary    content-type=application/json      authorization=${token}

    ${resp}     DELETE On Session      zp-api      /customers/${cpf}     headers=${headers}     expected_status=anything

    [Return]    ${resp}

##POST /equipos
Post Equipo
    [Arguments]     ${payload}

    Create Session      zp-api      ${base_api_url}

    ${token}=       Get Session Token
    
    &{headers}=     Create Dictionary    content-type=application/json      authorization=${token}

    ${resp}=        Post On Session     zp-api      /equipos      json=${payload}     headers=${headers}   expected_status=anything

    [Return]        ${resp}



