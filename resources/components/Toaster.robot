***Settings***
Documentation   Representação do Toaster que mostra msg no sistema

***Variables***
${TOASTER_SUCESS}    css:div[type=success] strong
${TOASTER_ERROR}     css:div[type=error] strong
${TOASTER_ERROR_P}     css:div[type=error] p