***Settings***

Library     OperatingSystem

***Keywords***
Get Json
    [Arguments]     ${file_name}

        ${json_File}             Get File        ${EXECDIR}/resources/fixtures/${file_name}
        ${json_dictionary}=      Evaluate        json.loads($json_File)     json

        [Return]       ${json_dictionary}