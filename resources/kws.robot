***Settings***
Library     SeleniumLibrary

***Keywords***
##Login
Acesso a pagina login
    Go To   http://zepalheta-web:3000

Submeto minhas credenciais
    [Arguments]     ${email}    ${password}

    Login with      ${email}    ${password}

Devo ver a área logada
    Wait Until Page Contains      Aluguéis      5

Devo ver um toaster com a mensagem
    [Arguments]     ${expected_message}

    Wait Until Element Contains     ${TOASTER_ERROR_P}     ${expected_message}

##Customers

Dado que acesso o formulario de cadastro de clientes
    Go To Customers

    Wait Until Element Is Visible    ${CUSTOMERS_FORM}    5
    Click Element                    ${CUSTOMERS_FORM}

E que eu tenho o seguinte cliente:
    [Arguments]            ${name}     ${cpf}      ${address}      ${phone_number}

    Remove Customer By Cpf      ${cpf}

    Set Test Variable      ${name}              
    Set Test Variable      ${cpf}
    Set Test Variable      ${address}
    Set Test Variable      ${phone_number}

Mas este cpf já existe no sistema
    Insert Customer  ${name}     ${cpf}      ${address}      ${phone_number}

Quando faço a inclusão desse cliente
    Register New Customer  ${name}     ${cpf}      ${address}      ${phone_number}

Então devo ver a notificação:
    [Arguments]     ${expected_message}

    Wait Until Element Contains     ${TOASTER_SUCESS}    ${expected_message}     5

E esse usuario deve ser exibido na lista
    ${cpf_formatado} =      Format Cpf      ${cpf}
    Go Back
    Wait Until Element is Visible   ${CUSTOMER LIST}       5
    Table Should Contain            ${CUSTOMER LIST}       ${cpf_formatado}

Então devo ver a notificação de erro:
    [Arguments]     ${expected_notice}

    Wait Until Element Contains     ${TOASTER_ERROR}    ${expected_notice}      5
    Click Element       css:button[type=button]


Entao devo ver mensagens informando que os campos do cadastro de clientes são Obrigatórios

    Wait Until Element Contains     ${LABEL_NAME}        Nome é obrigatório          5
    Wait Until Element Contains     ${LABEL_CPF}         CPF é obrigatório           5
    Wait Until Element Contains     ${LABEL_ADDRESS}     Endereço é obrigatório      5
    Wait Until Element Contains     ${LABEL_PHONE}       Telefone é obrigatório      5

Entao devo ver mensagen informando que o campo do cadastro de clientes é Obrigatório
    [Arguments]     ${expected_message}

    Wait Until Page Contains    ${expected_message}     5

## CADASTRO EQUIPOS

Dado que acesso o formulario de cadastro de equipamentos
    Wait Until Element Is Visible    ${NAV_EQUIPOS}       5
    Click Element                    ${NAV_EQUIPOS}

    Wait Until Element Is Visible    ${EQUIPOS_FORM}      5
    Click Element                    ${EQUIPOS_FORM}


E que eu tenho o seguinte equipamento:
    [Arguments]     ${name_equipo}      ${daily_price}

    Remove Equipo By Name   ${name_equipo}    

    Set Test Variable     ${name_equipo}  
    Set Test Variable     ${daily_price}
    
Quando faço a inclusão desse equipamento
    Register New Equipo     ${name_equipo}   ${daily_price}

Entao devo ver mensagen informando que os campos do cadastro de equipamentos é Obrigatório

    Wait until Element Contains     ${LABEL_NAME_EQUIPO}        Nome do equipo é obrigatório          5
    Wait until Element Contains     ${LABEL_DAILY_PRICE}        Diária do equipo é obrigatória        5

Entao devo ver mensagen informando que o campo do cadastro de equipamentos é Obrigatório
    [Arguments]     ${expected_message}
    Wait Until Page Contains    ${expected_message}     5

Mas este equipamento já existe no sistema
    Insert Equipo       ${name_equipo}   ${daily_price}

##REMOVE CUSTOMER

Dado que eu tenho um cliente indesejado:
    [Arguments]                 ${name}     ${cpf}      ${address}      ${phone_number}

    Remove Customer By Cpf                  ${cpf}
    Insert Customer             ${name}     ${cpf}      ${address}      ${phone_number}

    Set Test Variable           ${cpf}

E acesso a pagina a lista de clientes
    Go To Customers

Quando eu removo esse cliente
    ${format_cpf} =         Format Cpf      ${cpf}
    Set Test Variable       ${format_cpf}

    Go To Customer Details          ${format_cpf}
    Click Remove Customer

E esse cliente nao deve aparecer na lista
    Wait Until Page Does Not Contain       ${format_cpf}

## Contrato de locação

Dado que eu tenho o seguite cliente cadastrado:
    [Arguments]     ${file_name}

    ${customer}=     Get Json    customers/${file_name}

    Delete Customer                 ${customer['cpf']}
    ${resp}=         Post Customer  ${customer}

    Set Test Variable       ${customer}

E este cliente deseja alugar o seguinte equipo:
    [Arguments]     ${file_name}

    ${equipo}=     Get Json    equipos/${file_name}

    Post Equipo      ${equipo}

    Set Test Variable       ${equipo}

E acesso o formulario de contratos
    Go To Contracts
    Click Element       ${CONTRACTS_FORM}

Quando faço um novo contrato de Locação
    Create a new Contract  ${customer['name']}  ${equipo['name']}
