***Settings***
Documentation   Representação a pagina de equipos com seus elementos e ações

***Variables***
${EQUIPOS_FORM}             css:a[href$="register"]
${LABEL_NAME_EQUIPO}        css:label[for='equipo-name']        
${LABEL_DAILY_PRICE}        css:label[for='daily_price']  

***Keywords***
Register New Equipo
    [Arguments]     ${name_equipo}     ${daily_price}

    Reload Page

    Input Text  id:equipo-name      ${name_equipo}
    Input Text  id:daily_price      ${daily_price}


    Click Element   xpath://button[text()='CADASTRAR']