*** Settings ***                                                                                       

Resource    ../../../resources/services.robot

*** Test Cases ***                                                                                     
Login com Sucesso                                                                                      
    ${resp}=        Post Session  admin@zepalheta.com.br  pwd123

    Status Should Be    200    ${resp}

Senha incorreta   
    ${resp}=        Post Session  admin@zepalheta.com.br  pwd321

    Status Should Be             401    ${resp}

Usuario não existe 
    ${resp}=        Post Session  admin123@zepalheta.com.br  pwd321

    Status Should Be             401    ${resp}


                                        
