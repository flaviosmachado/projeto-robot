*** Settings ***                                                                                       

Resource    ../../../resources/services.robot

***Test Cases***
Get Customers List

    ${origin}=          Get Json  customers/chimbinha.json

    Delete Customer                     ${origin['cpf']}
    ${resp}=    Post Customer           ${origin}

    ${resp}=    Delete Customer         ${origin['cpf']}    
 
    Status Should Be    204             ${resp}

Customer Not Found

    ${resp}     Delete Customer    243.590.550-56

    Status Should Be    404     ${resp}
    Should Be Equal     ${resp.json()['message']}       Customer not found  