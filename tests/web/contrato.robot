***Settings***
Documentation       Cadastro de Contrato de Locação

Resource    ../../resources/base.robot


Suite Setup      Login Session
Suite Teardown   Finish Session
Test Teardown    Finish TestCase

***Test Cases***
Novo Contrato de Locação

    Dado que eu tenho o seguite cliente cadastrado:     adam.json
    E este cliente deseja alugar o seguinte equipo:     meteoro.json
    E acesso o formulario de contratos
    Quando faço um novo contrato de Locação
    Então devo ver a notificação:      Contrato criado com sucesso!
