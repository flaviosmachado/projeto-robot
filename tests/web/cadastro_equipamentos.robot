***Settings***
Documentation       Cadastro de equipamentos

Resource        ../../resources/base.robot

Test Setup      Login Session
Suite Teardown   Finish Session
Test Teardown   Finish TestCase

***Test Cases***
Novo equipamento
    Dado que acesso o formulario de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...     Guitarra      100  
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação:   Equipo cadastrado com sucesso!