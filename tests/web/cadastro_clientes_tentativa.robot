***Settings***
Documentation   Tentativa cadastro

Resource    ../../resources/base.robot

# executa uma ou mais Keywords somente uma vez antes de todos os casos de teste
Suite Setup      Login Session
# executa uma ou mais Keywords uma unica vez apos finalizar os casos de testes
Suite Teardown   Finish Session
Test Teardown   Finish TestCase

***Keywords***
Tentativa de cadastro
    [Arguments]     ${name}     ${cpf}      ${address}      ${phone_number}  ${output_mensagem}
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     ${name}    ${cpf}    ${address}    ${ phone_number}
    Quando faço a inclusão desse cliente
    Entao devo ver mensagen informando que o campo do cadastro de clientes é Obrigatório   ${output_mensagem}


***Test Cases***
Campos Obrigatórios
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Quando faço a inclusão desse cliente
    Entao devo ver mensagens informando que os campos do cadastro de clientes são Obrigatórios

Cliente duplicado
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     Adrian Smith    00000001506     Rua dos bugs, 2000      11999999999
    Mas este cpf já existe no sistema
    Quando faço a inclusão desse cliente
    Então devo ver a notificação de erro:   Este CPF já existe no sistema! 

Nome Obrigatório
    [Template]  Tentativa de cadastro
    ${EMPTY}    00000001406   Rua dos bugs, 1000    11999999999     Nome é obrigatório

Cpf Obrigatório
    [Template]   Tentativa de cadastro
    Bon Jovi    ${EMPTY}      Rua dos bugs, 1000    11999999999     CPF é obrigatório   

Endereco Obrigatório
    [Template]   Tentativa de cadastro
    Bon Jovi    00000001406   ${EMPTY}              11999999999     Endereço é obrigatório    

Telefone Obrigatório
    [Template]   Tentativa de cadastro
    Bon Jovi    00000001406   Rua dos bugs, 1000    ${EMPTY}        Telefone é obrigatório

Telefone incorreto
    [Template]   Tentativa de cadastro
    Bon Jovi    00000001406   Rua dos bugs, 1000    1199999999        Telefone inválido



