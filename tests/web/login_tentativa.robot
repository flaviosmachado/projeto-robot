***Settings***
Documentation   Login Tentativa

Resource    ../../resources/base.robot

# executa uma ou mais Keywords somente uma vez antes de todos os casos de teste
Suite Setup      Start Session
# executa uma ou mais Keywords uma unica vez apos finalizar os casos de testes
Suite Teardown   Finish Session

Test Template   Tentativa de login
Test Teardown   Finish TestCase


***Keywords***
Tentativa de login
    [Arguments]     ${input_email}      ${input_senha}      ${output_mensagem}

    Acesso a pagina login
    Submeto minhas credenciais  ${input_email}  ${input_senha}
    Devo ver um toaster com a mensagem  ${output_mensagem}

***Test Cases***
Senha Incorreta             admin@zepalheta.com.br  pwd124      Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco             admin@zepalheta.com.br  ${EMPTY}    O campo senha é obrigatório!
Email em branco             ${EMPTY}                pwd123      O campo email é obrigatório!
Email e senha em branco     ${EMPTY}                ${EMPTY}    Os campos email e senha não foram preenchidos!
Login incorreto             admin&gmail.com         pwd123      correu um erro ao fazer login, cheque as credenciais.
 



