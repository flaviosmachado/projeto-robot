***Settings***
Documentation   Cadastro de clientes

Resource    ../../resources/base.robot

Test Setup      Login Session
Suite Teardown   Finish Session
Test Teardown   Finish TestCase

***Test Cases***
Remover cliente

    Dado que eu tenho um cliente indesejado:
    ...     Bob Dylan       33333333333     Rua dos bugs, 3000      319888888888
    E acesso a pagina a lista de clientes
    Quando eu removo esse cliente
    Então devo ver a notificação:   Cliente removido com sucesso!
    E esse cliente nao deve aparecer na lista