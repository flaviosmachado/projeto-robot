***Settings***
Documentation   Tentativa cadastro equipamento

Resource    ../../resources/base.robot

# executa uma ou mais Keywords somente uma vez antes de todos os casos de teste
Suite Setup      Login Session
# executa uma ou mais Keywords uma unica vez apos finalizar os casos de testes
Suite Teardown   Finish Session
Test Teardown    Finish TestCase


**Keywords***
Tentativa de cadastro equipamentos
    [Arguments]     ${name_equipo}      ${daily_price}      ${output_mensagem}
    Dado que acesso o formulario de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...     ${name_equipo}    ${daily_price}
    Quando faço a inclusão desse equipamento
    Entao devo ver mensagen informando que o campo do cadastro de equipamentos é Obrigatório    ${output_mensagem}


***Test Cases***
Equipamento duplicado
    [Tags]  cadastro_equipo
    Dado que acesso o formulario de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...     Bateria     200
    Mas este equipamento já existe no sistema
    Quando faço a inclusão desse equipamento
    Então devo ver a notificação de erro:   Erro na criação de um equipo

Campos Obrigatórios
    [Tags]  cadastro_equipo
    Dado que acesso o formulario de cadastro de equipamentos
    E que eu tenho o seguinte equipamento:
    ...     ${EMPTY}    ${EMPTY}
    Quando faço a inclusão desse equipamento
    Entao devo ver mensagen informando que os campos do cadastro de equipamentos é Obrigatório

Nome Obrigatório
    [Tags]  cadastro_equipo
    [Template]      Tentativa de cadastro equipamentos
    ${EMPTY}    100     Nome do equipo é obrigatório

Valor Obrigatório
    [Tags]  cadastro_equipo
    [Template]      Tentativa de cadastro equipamentos
    Guitarra    ${EMPTY}       Diária do equipo é obrigatória