***Settings***
Documentation   Cadastro de clientes

Resource    ../../resources/base.robot

Test Setup      Login Session
Suite Teardown  Finish Session
Test Teardown   Finish TestCase

***Test Cases***
Novo CLiente
    [Tags]  smoke
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     Bon Jovi    00000001406     Rua dos bugs, 1000      11999999999
    Quando faço a inclusão desse cliente
    Então devo ver a notificação:  Cliente cadastrado com sucesso!
    E esse usuario deve ser exibido na lista